import asyncio
from multiprocessing import Process

import time
from threading import Thread
from sys import getrefcount


# def count_me(counter):
#     t1 = time.time()
#     iterator = 0
#     while iterator < counter:
#         iterator += 1
#     print(time.time() - t1)
#
#
# if __name__ == '__main__':
#     count = 100000000
#     time.sleep(2)
#     for x in range(0, 10):
#         process1 = Process(target=count_me, args=(count / 10,))
#         process1.start()

    # process2 = Process(target=count_me, args=(count / 4,))
    # process3 = Process(target=count_me, args=(count / 4,))
    # process4 = Process(target=count_me, args=(count / 4,))

    # process1.start()
    # process2.start()
    # process3.start()
    # process4.start()

    # print(getrefcount(count))
    # count_me(count)
    # thread1 = Thread(target=count_me, args=(count / 8,))
    # thread2 = Thread(target=count_me, args=(count / 8,))
    # thread3 = Thread(target=count_me, args=(count / 8,))
    # thread4 = Thread(target=count_me, args=(count / 8,))
    # thread5 = Thread(target=count_me, args=(count / 8,))
    # thread6 = Thread(target=count_me, args=(count / 8,))
    # thread7 = Thread(target=count_me, args=(count / 8,))
    # thread8 = Thread(target=count_me, args=(count / 8,))
    #
    # thread1.start()
    # thread2.start()
    # thread3.start()
    # thread4.start()
    # thread5.start()
    # thread6.start()
    # thread7.start()
    # thread8.start()


if __name__ == '__main__':
    count_range = 1000000

    async def count_me(count):
        for x in range(0, count):
            print(x)

    # print(count_me(1000000))
    loop = asyncio.get_event_loop()
    tasks = []

    for x in range(0, 10):
        tasks.append(loop.create_task(count_me(int(count_range / 10))))

    wait_tasks = asyncio.wait(tasks)
    t1 = time.time()
    loop.run_until_complete(wait_tasks)
    loop.close()
    print(time.time() - t1)

