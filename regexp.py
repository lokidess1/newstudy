import re

if __name__ == '__main__':
    text = """
    This is some text to find Some words there! And show
    That it's Easy to do. 10-10-2021 and +some more dates
    10-11-2020 and more 11-11-1111, and different +format
    10.11.1999 and +this one 11/04/1892 aaaaa 01-99-9999
"""
    # for word in text.split():
    #     if word.istitle():
    #         print(word)
    # print(re.findall('[A-Z][a-z]+', text))
    # print(re.findall('\d{4}|\d{2}', text))
    print(re.findall("[0-31]{2}.[0-12]{2}.\d{4}", text))

    text1 = input("Enter word from upper case: ")

    if re.match("^[A-Z][A-Za-z]+$", text1):
        print("Valid")
    else:
        print("Invalid")
