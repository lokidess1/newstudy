first_part = "Hello"
second_part = "World"

if __name__ == "__main__":
    # print(first_part + " " + second_part)
    # print("{} {}".format(first_part, second_part))
    # print("{1} {0} {1} {0}".format(first_part, second_part))
    # print("{hello} {world} {world} {hello}".format(
    #     hello=first_part, world=second_part
    # ))
    # print(f"{first_part + '-' * 100} {second_part}")
    # person = {
    #     'name': "Loki",
    #     'age': 31,
    #     'cats': [{'name': 'Thor'}, {'name': 'Kivi'}],
    #     (1, 2): "Tuple"
    # }
    # print(person[(1, 2)])

    # my_new_dict = dict.fromkeys(["name", "age", "skills"], "default")

    # a = "qwe"
    # b = "asd"
    #
    # a, b = b, a
    # a, *b, c = [10, 12, 43, 12, 1, 2, 5, 5]
    # print(a)
    # print(b)
    # print(c)

    # print(my_new_dict)
    # print(my_new_dict.items())
    # for key, value in my_new_dict.items():
    #     print(key, value, sep=': ')

    # print(my_new_dict.pop('asdadasd', None))
    # print(my_new_dict)
    # print(person.setdefault('qweqweqwe', 'default'))
    # print(person.get('asdsads'))

    # message = "Some message"
    # replaced = message.replace('s', 'S')
    # print(replaced, message, sep='|')
    #
    # lst = [1, 2, 3]
    # lst.reverse()
    # print(lst)
    # if False and False or True or False:
    #     print("Hello")
    # a = []
    # b = []
    # print(a == b)
    # print(a is b)

    a = "qwe"
    b = "qwe"

    # print(a == b)
    # print(a is b)

    # a = None
    # if a is None:
    #     pass

    names = ['Loki', 'Odin', 'Thor']

    names = iter(names)

    # next()

    print(next(names))
    print(next(names))
    print(next(names))
    print(next(names))

    # for number, name in enumerate(names, start=1):
    #     print(number, name)

    # for x in range(0, 10):
    #     print(x)
    #     if x == 9:
    #         break
    # else:
    #     print("Else HERE!")
