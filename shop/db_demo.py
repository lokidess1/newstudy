import sqlite3


if __name__ == '__main__':
    with sqlite3.connect('db.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT id, name, quantity, price, category FROM product")
        data = cursor.fetchall()
        print(data)
