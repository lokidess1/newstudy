import datetime
from tabulate import tabulate
from models import Product, Category


def add_category():
    name = input("Enter name: ")
    Category.create(name=name)


def list_category():
    categories = Category.select()
    data_output(categories)


def add_product():
    name = input("Enter name: ")
    price = input("Enter price: ")
    quantity = input("Enter quantity: ")
    category = input("Category name: ")

    category = Category.select().where(Category.name == category).get()  # TODO [0]

    Product.create(
        name=name,
        price=price,
        quantity=quantity,
        created_at=datetime.datetime.now(),
        category=category
    )


def delete_product():
    product_id = input("Enter product id: ")
    Product.delete().where(Product.id == product_id).execute()


def help_message():
    print("\n".join([f"{x[0]} - {x[1][1]}" for x in commands.items()]))


def data_output(data):
    if data:
        data = [x.__dict__['__data__'] for x in data]
        headers = data[0].keys()
        rows = [x.values() for x in data]
        print(tabulate(rows, headers))
    else:
        print('No data available')


def search():
    search_text = input("Enter search query: ")
    data_output(Product.select().where(Product.name.contains(search_text)))


def date_str_validator(date_str):
    date_parts = date_str.split('-')
    if len(date_parts) != 3:
        return False

    if not all([x.isdigit() for x in date_parts]):
        return False

    if len(date_parts[-1]) != 4:
        return False

    return True


def date_search():
    while True:
        start_date = input("Enter start date in format DD-MM-YYYY: ")
        if date_str_validator(start_date):
            break
        print("Enter valid value")

    while True:
        end_date = input("Enter end date in format DD-MM-YYYY: ")
        if date_str_validator(end_date):
            break
        print("Enter valid value")

    start_date = datetime.datetime.strptime(start_date, "%d-%m-%Y")
    end_date = datetime.datetime.combine(datetime.datetime.strptime(end_date, "%d-%m-%Y"), datetime.time.max)

    products = Product.select().where((Product.created_at >= start_date) & (Product.created_at <= end_date))
    data_output(products)


if __name__ == '__main__':

    commands = {
        'exit': (lambda: exit(0), "Finish the program"),
        'add': (add_product, "Add product"),
        'delete': (delete_product, "Remove product"),
        'list': (lambda: data_output(Product.select()), 'Show list of products'),
        'help': (help_message, 'Show help message'),
        'search': (search, "Search the item"),
        'df': (date_search, "Search between two dates"),
        'add_category': (add_category, "Add new category"),
        'lc': (list_category, "Show all categories")
    }

    while True:
        command = input('Enter command: ')
        commands[command][0]()
