class Animal:
    tail_length = 10
    name = "Murzik"
    prefix = "Animal"
    _some_protected = "Protected"
    __private = "Private"

    def __init__(self, name):
        self.name = name

    def walk(self):
        self.say("Rrrrrrrr")
        print(f"{self.prefix} {self.name} walking now")

    def say(self, message):
        print(f"{self.prefix} say: {message}")

    def __del__(self):
        print(f"{self.prefix} {self.name} is removed from memory")


class Dog:
    tail = 4
    prefix = 'Dog'

    def roar(self):
        print(f"{self.prefix} roar now!")

    def jump(self):
        print(f"{self.prefix} jump like a dog!")


class Cat(Animal):
    prefix = 'Cat'

    def walk(self):
        print(f"{self.prefix} walking now")

    def jump(self):
        print(f"{self.prefix} jup now")


class CatDog(Cat, Dog):
    pass


if __name__ == '__main__':
    animal = CatDog(name="Kiwi")
    # print(animal._Animal__private)

    animal.qweqweqw = "qweqweqwe"

    print(CatDog.__mro__)
    animal.walk()
    animal.jump()
    animal.name = "New name"
    print(animal.name)
    print(animal.qweqweqw)

