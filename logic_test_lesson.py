if __name__ == '__main__':
    first = 10
    second = 15
    third = 5

    lst = [1, 2, 3, 4, 5, 15]
    dct = {
        "name": "Loki",
        'age': 17
    }

    user_data = int(input("Enter number: "))

    if user_data <= first:
        print("First logical part")
        if user_data >= third:
            print("Nested logical part")

    # elif user_data > first and user_data < second:
    #     print("Second logical part")
    # else:
    #     print("Value out of range")

    # print(first > second or second > third and third < first)
    # print(second in lst)
    # print("qwe" in dct)
