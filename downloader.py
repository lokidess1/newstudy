import asyncio
from threading import Thread

import time
import requests
from aiohttp import ClientSession, web
from multiprocessing import Process
import aiofiles


URL = 'https://loremflickr.com/600/1024'


async def download(count, prefix=''):
    for item in range(count):
        async with ClientSession() as session:
            response = await session.get(URL)
            async with aiofiles.open(f'images/image{prefix}_{item}.jpg', 'wb') as image_file:
                await image_file.write(await response.read())


def sync_download(count, prefix=''):
    t1 = time.time()
    for item in range(count):
        response = requests.get(URL)
        with open(f'images/image{prefix}_{item}.jpg', 'wb') as image_file:
            image_file.write(response.content)
    print(time.time() - t1)


if __name__ == '__main__':
    # t1 = time.time()
    # download(10)

    # loop = asyncio.get_event_loop()
    # tasks = []
    #
    # for x in range(10):
    #     tasks.append(loop.create_task(download(10, f'_{x}')))
    #
    # awaited_tasks = asyncio.wait(tasks)
    # loop.run_until_complete(awaited_tasks)
    # loop.close()

    # print(time.time() - t1)
    # for x in range(10):
    #     Process(target=sync_download, args=(10, f'_{x}')).start()
    app = web.Application()

    # localhost == 127.0.0.1
    web.run_app(app, host='0.0.0.0')

