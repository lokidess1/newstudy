def is_even(number_to_check):
    if number_to_check.isdigit():
        number_to_check = int(number_to_check)
        if number_to_check % 2 == 0:
            return True
        return False


if __name__ == '__main__':

    placeholder = {
        True: "Even",
        False: "Odd",
        None: "Not Valid"
    }

    while True:
        data = {}
        values = input("Enter comma separated values: ").strip(',')
        if values:
            data = dict([(value, placeholder[is_even(value)]) for value in values.split(',')])
        print(data)

    # lst = [1, 2, 3, 4, 5, 6]
    # some = lst[0] if 10 > 2 else lst[-1]
    # result = [x + 1 if x % 2 == 0 else x for x in lst]
    # result_map = map(lambda x: x + 1, lst)
    # for item in lst:
    #     result.append(item + 1)
    # print(list(result_map))
    # print(result)
