from django import forms
from core.models import Category, Tags, Product


# class ProductForm(forms.Form):
#     name = forms.CharField(required=True)
#     price = forms.DecimalField()
#     category = forms.ModelChoiceField(queryset=Category.objects.all())
#     description = forms.CharField(required=False, widget=forms.widgets.Textarea)
#     tags = forms.ModelMultipleChoiceField(queryset=Tags.objects.all(), widget=forms.widgets.CheckboxSelectMultiple)
#
#     def save(self):
#         product = Product.objects.create(
#             name=self.cleaned_data['name'],
#             price=self.cleaned_data['price'],
#             category=self.cleaned_data['category'],
#             description=self.cleaned_data['description'],
#         )
#         product.tags.add(*self.cleaned_data['tags'])
#         return product

class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        # fields = ['name', 'price']
        fields = '__all__'
