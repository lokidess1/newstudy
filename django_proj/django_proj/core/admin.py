from django.contrib import admin

from core.models import Category, Product, Tags


admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Tags)
