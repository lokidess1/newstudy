from django.db import models
# from django.contrib.postgres.fields.array import ArrayField


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    category = models.ForeignKey('core.Category', on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)
    tags = models.ManyToManyField('core.Tags', blank=True)

    def __str__(self):
        return self.name


class Tags(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
