from django.shortcuts import render, redirect
from django.views.generic.base import View, TemplateView
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import FormView, CreateView
from core.models import Product, Category
# from core.forms import ProductForm


def index(request):
    return render(template_name='index.html', request=request, context={
        
    })


class IndexView(TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        # category = Category.objects.filter(id=2).first()
        # context['products'] = Product.objects.filter(category__id=2)
        context['products'] = Product.objects.filter(name="qwe").order_by(
            '-category__name'
        ).select_related('category').prefetch_related('tags')
        return context

    # @csrf_exempt
    # def dispatch(self, request, *args, **kwargs):
    #     return super(IndexView, self).dispatch(request, *args, **kwargs)

    # def get(self, request):
    #     print('This is GET')
    #     return render(template_name='index.html', request=request)
    #
    # def post(self, request):
    #     print('This is POST')
    #     return render(template_name='index.html', request=request)
    #
    # def put(self, request):
    #     return JsonResponse({'message': 'this is PUT'})


class SimpleObj:

    def say_hello(self):
        return "Hello World"

    def say_custom_hello(self, prefix):
        return f"{prefix}, hello!"


class AnotherIndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(AnotherIndexView, self).get_context_data(**kwargs)
        context['greet'] = "HELLO!"
        context['lst'] = [1, 2, 3, 4]
        context['dct'] = {
            'name': "Loki",
            'age': 31
        }
        context['obj'] = SimpleObj()
        # print(self.request)
        return context

    # def post(self, request):
    #     print('This is POST')
    #     return self.render_to_response(context={})


class AboutUs(TemplateView):
    template_name = "about.html"


# class CreateProductView(TemplateView):
#     template_name = 'create_product.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(CreateProductView, self).get_context_data(**kwargs)
#         context['form'] = ProductForm()
#         return context
#
#     def post(self, request):
#         form = ProductForm(data=request.POST)
#         context = self.get_context_data()
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#
#         context['form'] = form
#         return self.render_to_response(context=context)

# class CreateProductView(FormView):
#     template_name = 'create_product.html'
#     form_class = ProductForm
#     success_url = '/'
#
#     def form_valid(self, form):
#         form.save()
#         return super(CreateProductView, self).form_valid(form)

    # def form_invalid(self, form):
    #     pass


class CreateProductView(CreateView):
    template_name = 'create_product.html'
    model = Product
    fields = '__all__'
    success_url = '/'


# """
# SELECT "core_tags"."id",
#        "core_tags"."name"
#   FROM "core_tags"
#  INNER JOIN "core_product_tags"
#     ON ("core_tags"."id" = "core_product_tags"."tags_id")
#  WHERE "core_product_tags"."product_id" in ['1', '2', '3']
# """
