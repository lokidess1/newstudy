import datetime
from wtforms import Form, StringField, validators, DecimalField, IntegerField, SelectField, widgets
from models import Category, Product


class ProductForm(Form):
    name = StringField(validators=[validators.DataRequired()])
    price = DecimalField(validators=[validators.DataRequired()])
    quantity = IntegerField(validators=[validators.DataRequired()])
    description = StringField(validators=[validators.Optional()], widget=widgets.TextArea())
    category = SelectField(
        validators=[validators.DataRequired()],
        choices=[(x.id, x.name) for x in Category.select()],
    )

    def save(self):
        category = Category.select().where(Category.id == self.category.data).get()

        Product.create(
            name=self.name.data,
            price=self.price.data,
            quantity=self.quantity.data,
            description=self.description.data,
            category=category,
            created_at=datetime.datetime.now()
        )
