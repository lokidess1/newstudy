from peewee import (PostgresqlDatabase, Model, AutoField, CharField, DecimalField,
                    IntegerField, DateTimeField, TextField, ForeignKeyField)

db = PostgresqlDatabase(
    "store",
    host="database",
    port=5432,
    password="secret",
    user="loki"
)


class Category(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255, unique=True)

    class Meta:
        database = db


class Product(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)
    price = DecimalField(max_digits=50, decimal_places=2)
    quantity = IntegerField()
    description = TextField(null=True)
    created_at = DateTimeField()
    category = ForeignKeyField(Category, backref="products")

    class Meta:
        database = db


if __name__ == '__main__':
    db.create_tables([Category, Product])
