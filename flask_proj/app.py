import os
from flask import Flask, render_template, request, redirect
from models import Product
from forms import ProductForm

app = Flask(__name__)


# print(os.getenv("TEST"))

@app.route('/')
def home_page():
    products = Product.select()
    return render_template('index.html', products=products)


@app.route('/product/add', methods=['GET', 'POST'])
def add_product():
    # errors = {}
    # if request.method == 'POST':
    #     if not re.match('\w+@\w+\.com', request.form['email']):
    #         errors['email'] = "ENTER VALID EMAIL"
    form = ProductForm()
    if request.method == 'POST':
        form = ProductForm(formdata=request.form)
        if form.validate():
            form.save()
            return redirect('/', 302)

    return render_template('add_product.html', form=form)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
